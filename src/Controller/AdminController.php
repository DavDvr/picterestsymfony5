<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditUserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_ADMIN")
 * Class AdminController
 * @package App\Controller
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="app_index")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/user", name="app_list_user")
     * @param UserRepository $userRepo
     * @return Response
     */
    public function usersList(UserRepository $userRepo): Response
    {
        return $this->render('admin/users.html.twig', [
            'users' => $userRepo->findAll(),
        ]);
    }

    /**
     * @param User $user
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/user/edit/{id}", name="app_edit_user")
     */
    public function editUser(User $user, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(EditUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'You\'ve edit successfully user');
            return $this->redirectToRoute('admin_app_list_user');
        }


        return $this->render('admin/editUser.html.twig', [
            'user'=>$user->getFullName(),
            'userRole'=>$this->getUser()->getRoles(),
            'userForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete_by_admin", methods={"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param User $user
     * @return Response
     */
    public function deleteUserByAdmin(Request $request,EntityManagerInterface $em,User $user): Response
    {
        if ($this->isCsrfTokenValid('delete_user'.$user->getId(), $request->request->get('_token'))) {
            $this->container->get('security.token_storage')->setToken(null);
            $em->remove($user);
            $em->flush();
            $this->redirectToRoute('admin_app_list_user');
            $this->addFlash('success','Account is successfully delete');
        }

        return $this->redirectToRoute('app_home');
    }

}
