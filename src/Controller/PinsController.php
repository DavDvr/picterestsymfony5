<?php

namespace App\Controller;

use App\Entity\Pin;
use App\Form\PinType;
use App\Repository\PinRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PinsController extends AbstractController
{

    /**
     * @Route("/", name="app_home", methods="GET")
     * @param PinRepository $pinRepository
     * @return Response
     */
    public function index(PinRepository $pinRepository): Response
    {
         $pins = $pinRepository->findBy([],['createdAt'=>'DESC']);
        return $this->render('pins/index.html.twig', compact('pins'));
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/pins/create", name="app_pin_create", methods={"GET", "POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $em) : Response
    {
        $pin = new Pin();
        $form = $this->createForm(PinType::class, $pin);
        //récupère les données du formulaires via les requêtes
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $pin->setUser($this->getUser());
            $em->persist($pin);
            $em->flush();

            $this->addFlash('success','Pin successfully created!');
            return $this->redirectToRoute('app_home');

        }
        return $this->render('pins/create.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * la route est le pins suivi de l'id avec une obligation d'être des chiffres
     * @Route("/pins/{id<[0-9]+>}", name="app_pins_show", methods="GET")
     * @param Pin $pin //va prendre l'id
     * @return Response
     */
    public function show(Pin $pin): Response
    {
        $user =$this->getUser();
        return $this->render('pins/show.html.twig', ['pin'=>$pin, 'user'=>$user]);
    }

    /**
     * @Route("/pins/{id<[0-9]+>}/edit", name="app_pins_edit", methods={"GET", "PUT", "POST"})
     * @param Pin $pin
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function edit(Pin $pin, Request $request,EntityManagerInterface $em): Response
    {


        if (($pin->getOwner() !== $this->getUser()) and ($this->getUser()->getRoles() == ['ROLE_USER'])) {
//            throw $this->createAccessDeniedException();

            $this->addFlash('error','Access Denied');
            return $this->redirectToRoute('app_home');
        }

        //je fais appel au formulaire que j'ai créé via la commande make:form,
        // puis je lui dis d'utiliser la methode PUT qui est recommandée pour edit
        $form = $this->createForm(PinType::class, $pin, [
            'method'=> 'PUT'
        ]);
        //récupère les données du formulaires via les requêtes
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em->persist($pin);
            $em->flush();
            $this->addFlash('success','Pin successfully updated!');
            return $this->redirectToRoute('app_home');

        }
        return $this->render('pins/edit.html.twig', [
            'pin'=>$pin,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/pins/{id<[0-9]+>}", name="app_pins_delete", methods={"DELETE"})
     * @param Request $request
     * @param Pin $pin
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete(Request $request, Pin $pin, EntityManagerInterface $em): Response
    {


        if (($pin->getOwner() !== $this->getUser()) and ($this->getUser()->getRoles() == ['ROLE_USER'])) {

            $this->addFlash('error','Access Denied');
            return $this->redirectToRoute('app_home');
        }
        if($this->isCsrfTokenValid('pin_deletion_'. $pin->getId(),$request->request->get('csrf_token'))){
            $em->remove($pin);
            $em->flush();
        }
        $this->addFlash('info','Pin successfully deleted!');
        return $this->redirectToRoute('app_home');
    }

}
