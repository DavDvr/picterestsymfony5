<?php


namespace App\Controller;


use App\Form\SearchPinType;
use App\Repository\PinRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_USER")
 * Class SearchController
 * @package App\Controller
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/pin/search", name="search_pin")
     * @param Request $request
     * @param PinRepository $pinRepository
     * @param EntityManagerInterface $em
     * @return mixed
     */
    public function search(Request $request, PinRepository $pinRepository, EntityManagerInterface $em) :Response
    {
        $pins=[];
        $searchPinForm= $this->createForm(SearchPinType::class);
        $searchPinForm->handleRequest($request);

        if($searchPinForm->isSubmitted() and $searchPinForm->isValid())
        {
            $criteria = $searchPinForm->getData();
           $pins = $pinRepository->searchByCategory($criteria);

            if ($pins == null)
            {
                $this->redirectToRoute('search_pin');
                $this->addFlash('info','No pins for your research');
            }else{
                $pin= count($pins)>=2 ?  'pins' : 'pin';

                $this->addFlash('success','You\'ve '.count($pins).' '.$pin.' for your research, you can see if you slide this page');
            }
        }
        return $this->render('search/search.html.twig', [
            'pins'=>$pins,
            'searchForm'=>$searchPinForm->createView()
        ]);
    }
}