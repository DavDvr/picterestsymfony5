<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="app_category")
     * @param CategoryRepository $repository
     * @return Response
     */
    public function listCategory(CategoryRepository $repository): Response
    {
        $cat =$repository->findAll();

        return $this->render('category/index.html.twig', compact('cat'));
    }

    /**
     * @Route("/category/create", name="app_category_create", methods={"GET", "POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $cat = new Category();
        $form= $this->createForm(CategoryType::class, $cat);
        //récupère les données du formulaire via les requêtes
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em->persist($cat);
            $em->flush();

            $this->addFlash('info','Category successfully created, you can find it here!');
            return $this->redirectToRoute('app_pin_create');

        }
        return $this->render('category/create.html.twig',[
            'form'=>$form->createView()
        ]);
    }

}
