<?php

namespace App\Controller;

use App\Entity\Pin;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\PinRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_USER")
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/account", name="user_index", methods={"GET"})
     * @param UserRepository $userRepository
     * @param PinRepository $pinRepository
     * @return Response
     */
    public function account(UserRepository $userRepository,PinRepository $pinRepository): Response
    {
           $accountUser = $userRepository->find($this->getUser());

        return $this->render('user/index.html.twig', [
            'users' => $accountUser,
        ]);

    }


    /**
     * @Route("/{id}", name="show_pin_user", methods={"GET"})
     */
    public function show(): Response
    {
        $user=$this->getUser();

        return $this->render('user/show.html.twig', [
            'user'=>$user
        ]);
    }
    /**
     * @Route("/{id}/listPinAccount", name="list_pin_account", methods={"GET"})
     */
    public function listPinAccount(): Response
    {
        $user=$this->getUser();
        $pinListUser= $this->getDoctrine()->getRepository('App:Pin')->findBy(['user'=>$this->getUser()],['createdAt'=>'DESC']);

        return $this->render('user/listPinAccount.html.twig', [
            'pins'=>$pinListUser,
            'user'=>$user
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete(Request $request, User $user, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
                $this->container->get('security.token_storage')->setToken(null);
                $em->remove($user);
                $em->flush();
                $this->redirectToRoute('app_login');
                $this->addFlash('success','Your account is successfully delete');

        }

        return $this->redirectToRoute('app_home');
    }
}
