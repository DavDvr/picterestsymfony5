<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Pin;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PinType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('category', EntityType::class,[
                'class'=>Category::class,
                'required'=>true,
                'placeholder'=>'choose an option',
                'choice_label'=>'nameCategory',
                'multiple'=>false,
                'expanded'=>false,

            ])
            ->add('imageFile', VichImageType::class, [
                'label'=>'Image',
                'required' => false,
                'allow_delete' => true,
                'delete_label' => 'Delete?',
                'download_label' => 'Download Image',
                'download_uri' => false,
                'imagine_pattern' => 'squared_thumbnail_small',

            ])

            ->add('title', TextType::class)
            ->add('description',TextareaType::class);


    }

    /**
     * @param OptionsResolver $resolver
     * configure les options du formulaire, par défaut nous lui passons l'entité Pin
     * de ce fait, il va regarder dans l'entité le type des propriétés que nous avons passé lors de la creation de l'entité
     * je n'ai plus besoin de specifier dans le formulaire ci-dessus que description est de type textarea
     * il verra que c'est une propriété de type text donc il va le gérer automatiquement
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pin::class,
        ]);
    }
}
