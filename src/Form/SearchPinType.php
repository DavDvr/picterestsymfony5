<?php


namespace App\Form;


use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchPinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder
           ->add('title', TextType::class,[
               'required'=>false
           ])
           ->add('category', EntityType::class,[
               'class'=>Category::class,
               'required'=>false,
               'placeholder'=>'choose an category',
               'choice_label'=>'nameCategory',
               'empty_data'=>'Tarzan',
               'multiple'=>false,
               'expanded'=>false,

           ]);
    }
}