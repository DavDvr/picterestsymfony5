<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pluralize', [$this, 'doSomething']),
        ];
    }


    /**
     * @param int $count
     * @param string $singular
     * @param string|null $plural
     * @return string
     * fonction permettant de mettre au singulier ou au pluriel mes pins en fonction du nombre de pins existant
     */
    public function doSomething(int $count, string $singular, ?string $plural=null): string
    {
        $plural ??= $singular . 's';
        $result = $count===1 ? $singular : $plural;
        return "$count $result";
    }
}
